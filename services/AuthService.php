<?php

namespace app\services;

use app\core\Controller;
use app\models\LoginForm;

class AuthService extends Controller
{

    public function login($response)
    {
        $loginForm = new LoginForm();
        if ($this->isPost()) {
            $loginForm->loadData($this->getBody());
            if ($loginForm->validate() && $loginForm->login()) {
                $response->redirect('/');
                return;
            }
        }
        return $loginForm;
    }

    public function renderModel($login)
    {
        return $this->render('login', [
            'model' => $login
        ]);
    }
}
