<?php

namespace app\core;

use app\models\Post;
use app\models\User;

class Application
{

    public static string $ROOT_DIR;

    public string $userClass;

    public string $layout = 'main';
    public Router $router;
    public Response $response;
    public Session $session;
    public Database $db;
    public ?User $user = null;
    public ?Post $post = null;
    public View $view;
    public static array $posts = [];

    public static Application $app;
    public ?Controller $controller = null;

    public function __construct($rootPath, array $config)
    {
        self::$ROOT_DIR = $rootPath;
        self::$app = $this;
        $this->router = new Router();
        $this->response = new Response();
        $this->session = new Session();
        $this->view = new View();

        $this->db = new Database($config['db']);

        $primaryValue = $this->session->get('user');
        if ($primaryValue) {
            $primaryKey = User::primaryKey();
            $this->user = User::findOne([$primaryKey => $primaryValue]);
        } else {
            $this->user = null;
        }

        $postValue = Application::$app->session->get('post');
        if ($postValue) {
            $postKey = Post::postKey();
            self::$posts = Post::findPosts([$postKey => $postValue]);
        }
    }

    public function run()
    {
        try {
            echo $this->router->resolve();
        } catch (\Exception $e) {
            Application::$app->response->setStatusCode($e->getCode());
            echo $this->view->renderView('error', [
                'exeption' => $e
            ]);
        }
    }

    public function getController()
    {
        return $this->controller;
    }

    public function setController(Controller $controller)
    {
        $this->controller = $controller;
    }

    public function login(DbModel $user)
    {
        $this->user = $user;
        $primaryKey = $user->primaryKey();
        $primaryValue = $user->{$primaryKey};
        $this->session->set('user', $primaryValue);
        $this->session->set('post', $primaryValue);
        return true;
    }

    public function logout()
    {
        $this->user = null;
        $this->session->remove('user');
    }

    public static function isGuest()
    {
        return !self::$app->user;
    }
}
