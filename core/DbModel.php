<?php

namespace app\core;

abstract class DbModel extends Model
{

    abstract public function attributes();
    abstract public function updateAttributes();
    abstract public function tableName();

    public static function primaryKey()
    {
        return 'id';
    }

    public function save()
    {
        $tableName = $this->tableName();
        $attributes = $this->attributes();
        $params = array_map(fn($attr) => ":$attr", $attributes);
        $statement = self::prepare("INSERT INTO $tableName (" . implode(',', $attributes) . ") 
            VALUES(" . implode(',', $params) . ")");
        foreach ($attributes as $attribute) {
            if ($attribute === 'photo') {
                $this->{$attribute} = $_FILES['photo']['name'];
            }
            $statement->bindValue(":$attribute", $this->{$attribute});
        }
        File::uploadToFolder();
        $statement->execute();
        return true;
    }

    public function saveUpdate()
    {
        $tableName = $this->tableName();
        $attributes = $this->updateAttributes();
        $params = array_map(fn($attr) => ":$attr", $attributes);
        $statement = self::prepare("UPDATE $tableName SET " .
            $attributes[0] . "=" . $params[0] . "," .
            $attributes[1] . "=" . $params[1] . "," .
            $attributes[2] . "=" . $params[2] . "," .
            $attributes[3] . "=" . $params[3] .
            " WHERE id=" . Application::$app->user->id);
        foreach ($attributes as $attribute) {
            if ($attribute === 'photo' && !empty($_FILES)) {
                File::deleteFile();
                $this->{$attribute} = $_FILES['photo']['name'];
            }
            $statement->bindValue(":$attribute", $this->{$attribute});
        }
        File::uploadToFolder();
        $statement->execute();
        return true;
    }

    public static function prepare($sql)
    {
        return Application::$app->db->pdo->prepare($sql);
    }

    public static function findOne($where)
    {
        $tableName = 'users';
        $statement = self::sqlRequest($where, $tableName);
        return $statement->fetchObject(static::class);
    }

    public static function findPosts($where)
    {
        $tableName = 'posts';
        $statement = self::sqlRequest($where, $tableName);
        return $statement->fetchAll();
    }

    public static function sqlRequest($where, $tableName)
    {
        $attributes = array_keys($where);
        $sql = implode("AND", array_map(fn($attr) => "$attr = :$attr", $attributes));
        $statement = self::prepare("SELECT * FROM $tableName WHERE $sql");
        foreach ($where as $key => $item) {
            $statement->bindValue(":$key", $item);
        }

        $statement->execute();
        return $statement;
    }
}
