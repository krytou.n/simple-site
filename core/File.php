<?php

namespace app\core;

class File
{
    public static function uploadToFolder()
    {
        move_uploaded_file($_FILES['photo']['tmp_name'], "storage/" . $_FILES['photo']['name']);
    }

    public static function deleteFile()
    {
        unlink('storage/' . Application::$app->user->photo);
    }
}
