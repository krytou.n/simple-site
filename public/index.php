<?php

error_reporting(E_ALL);
ini_set('display_errors', 'on');

require_once __DIR__ . '/../vendor/autoload.php';

use app\controllers\AuthController;
use app\controllers\CreatePostController;
use app\controllers\EditController;
use app\controllers\RegistController;
use app\controllers\SiteController;
use app\core\Application;

$dotenv = Dotenv\Dotenv::createImmutable(dirname(__DIR__));
$dotenv->load();

$config = [
    'userClass' => 'User',
    'db' => [
        'dsn' => $_ENV['DB_DSN'],
        'user' => $_ENV['DB_USER'],
        'password' => $_ENV['DB_PASSWORD'],
    ]
];

$app = new Application(dirname(__DIR__), $config);

$app->router->get('/', [SiteController::class, 'home']);
$app->router->get('/post', [SiteController::class, 'post']);
$app->router->get('/login', [AuthController::class, 'login']);
$app->router->post('/login', [AuthController::class, 'login']);
$app->router->get('/register', [RegistController::class, 'register']);
$app->router->post('/register', [RegistController::class, 'register']);
$app->router->get('/logout', [AuthController::class, 'logout']);
$app->router->get('/profile', [SiteController::class, 'profile']);
$app->router->get('/edit', [EditController::class, 'edit']);
$app->router->post('/edit', [EditController::class, 'edit']);
$app->router->get('/createPost', [CreatePostController::class, 'createPost']);
$app->router->post('/createPost', [CreatePostController::class, 'createPost']);

$app->run();
