<?php

namespace app\models;

use app\core\DbModel;

class User extends DbModel
{

    public const STATUS_INACTIVE = 0;
    public const STATUS_ACTIVE = 1;
    public const STATUS_DELETED = 2;

    public string $firstname = '';
    public string $lastname = '';
    public string $email = '';
    public int $status = self::STATUS_INACTIVE;
    public string $photo = '';
    public string $password = '';
    public string $passwordConfirm = '';

    public function tableName()
    {
        return 'users';
    }

    public function attributes()
    {
        return ['firstname', 'lastname', 'email', 'photo', 'password', 'status'];
    }

    public function updateAttributes()
    {
        return ['firstname', 'lastname', 'email', 'photo'];
    }

    public function save()
    {
        $this->status = self::STATUS_INACTIVE;
        $this->password = password_hash($this->password, PASSWORD_DEFAULT);
        return parent::save();
    }

    public function rules()
    {
        return [
            'firstname' => [self::RULE_REQUIRED],
            'lastname' => [self::RULE_REQUIRED],
            'email' => [self::RULE_REQUIRED, self::RULE_EMAIL, [
                self::RULE_UNIQUE, 'class' => self::class
                ]],
            'password' => [self::RULE_REQUIRED, [self::RULE_MIN, 'min' => 8], [self::RULE_MAX, 'max' => 24]],
            'passwordConfirm' => [self::RULE_REQUIRED, [self::RULE_MATCH, 'match' => 'password']],
        ];
    }

    public function labels()
    {
        return [
            'firstname' => 'First name',
            'lastname' => 'Last name',
            'email' => 'Email',
            'file' => 'File',
            'password' => 'Password',
            'passwordConfirm' => 'Confirm password',
        ];
    }

    public function getDisplayName()
    {
        return $this->firstname . ' ' . $this->lastname;
    }

    public function getDisplayStatus()
    {
        switch ($this->status) {
            case 0:
                return 'Inactive';
            break;
            case 1:
                return 'Active';
            break;
            case 2:
                return 'Delete';
            break;
        }
    }
}
