<?php

namespace app\models;

use app\core\Application;
use app\core\DbModel;

class Post extends DbModel
{

    public string $title = '';
    public string $text = '';
    public int $userId = 0;

    public function tableName()
    {
        return 'posts';
    }

    public static function postKey()
    {
        return 'userId';
    }

    public function attributes()
    {
        return ['title', 'text', 'userId'];
    }

    public function updateAttributes()
    {
        return ['title', 'text', 'userId'];
    }

    public function rules()
    {
        return [
            'title' => [self::RULE_REQUIRED],
            'text' => [self::RULE_REQUIRED, self::RULE_UNIQUE],
        ];
    }

    public function save()
    {
        $tableName = $this->tableName();
        $attributes = $this->attributes();
        $params = array_map(fn($attr) => ":$attr", $attributes);
        $statement = self::prepare("INSERT INTO $tableName (" . implode(',', $attributes) . ") 
            VALUES(" . implode(',', $params) . ")");
        foreach ($attributes as $attribute) {
            if ($attribute === 'userId') {
                $this->{$attribute} = Application::$app->user->id;
            }
            $statement->bindValue(":$attribute", $this->{$attribute});
        }
        $statement->execute();
        return true;
    }

    public function getTitle()
    {
        return $this->title;
    }
}
