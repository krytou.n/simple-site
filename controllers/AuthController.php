<?php

namespace app\controllers;

use app\core\Application;
use app\core\Controller;
use app\core\middlewares\AuthMiddleware;
use app\core\Response;
use app\services\AuthService;

class AuthController extends Controller
{

    public function __construct()
    {
        $this->registerMiddleware(new AuthMiddleware(['profile']));
    }

    public function login(Response $response)
    {
        $login = new AuthService();
        $loginParam = $login->login($response);
        if ($loginParam) {
            $this->setLayout('auth');
            return $login->renderModel($loginParam);
        }
    }

    public function logout(Response $response)
    {
        Application::$app->logout();
        $response->redirect('/');
    }
}
