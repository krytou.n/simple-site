<?php

namespace app\controllers;

use app\core\Application;
use app\core\Controller;
use app\models\User;

class EditController extends Controller
{
    public function edit()
    {
        $user = new User();
        if ($this->isPost()) {
            $user->loadData($this->getBody());
            if (/*$user->validate() &&*/ $user->saveUpdate()) {
                Application::$app->session->setFlash('success', 'Data update successfully');
                Application::$app->response->redirect('/');
            }

            $this->setLayout('auth');
            return $this->render('register', [
                'model' => $user
            ]);
        }

        $this->setLayout('auth');
        return $this->render('edit', [
            'model' => $user
        ]);
    }
}
