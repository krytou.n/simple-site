<?php

namespace app\controllers;

use app\core\Application;
use app\core\Controller;
use app\models\User;

class RegistController extends Controller
{
    public function register()
    {
        $user = new User();
        if ($this->isPost()) {
            $user->loadData($this->getBody());
            if ($user->validate() && $user->save()) {
                Application::$app->session->setFlash('success', 'Thanks for registering');
                Application::$app->response->redirect('/');
            }

            $this->setLayout('auth');
            return $this->render('register', [
                'model' => $user
            ]);
        }
        $this->setLayout('auth');
        return $this->render('register', [
            'model' => $user
        ]);
    }
}
