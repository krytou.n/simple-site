<?php

namespace app\controllers;

use app\core\Controller;

class SiteController extends Controller
{
    public function post()
    {
        return $this->render('post');
    }

    public function home()
    {
        return $this->render('home');
    }

    public function profile()
    {
        return $this->render('profile');
    }
}
