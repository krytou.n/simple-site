<?php

namespace app\controllers;

use app\core\Application;
use app\core\Controller;
use app\models\Post;

class CreatePostController extends Controller
{
    public function createPost()
    {
        $post = new Post();
        if ($this->isPost()) {
            $post->loadData($this->getBody());
            if ($post->save()) {
                Application::$app->session->setFlash('success', 'Post create successfully');
                Application::$app->response->redirect('/post');
            }

            $this->setLayout('auth');
            return $this->render('register', [
                'model' => $post
            ]);
        }

        $this->setLayout('auth');
        return $this->render('createPost', [
            'model' => $post
        ]);
    }
}
