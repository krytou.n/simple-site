<?php

use app\core\Application;

$this->title = 'Posts';
?>

<?php if (!Application::isGuest()) : ?>
    <a href="/createPost" class="btn btn-primary btn-lg active" role="button" aria-pressed="true">Create a new post</a>
<?php endif; ?>

<div class="container">
<div class="col-md-12">
    <?php if (!Application::isGuest()) : ?>
        <?php foreach (Application::$posts as $d) : ?>
        <h1><?= $d['title']; ?></h1>
        <p><?= $d['text']; ?></p>
        <div>
            <span>Posted <?= $d['created_at'];?></span>
        </div>
        <hr>
        <?php endforeach; ?>
    <?php endif; ?>
</div>
</div>