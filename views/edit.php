<?php

use app\core\Application;
use app\core\form\Form;

?>
<h1>Edit</h1>
<?php
$form = Form::begin('', "post");
?>

<div class="container">
    <div class="main-body">
    	<div class="row">
        	<div class="col-lg-4">
				<div class="card">
						<div class="card-body">
							<div class="d-flex flex-column align-items-center text-center">
								<img src="/storage/<?= Application::$app->user->photo;?>" alt="Admin" class="rounded-circle p-1 bg-primary" width="110">
								<div class="mt-3">
									<h4><?= Application::$app->user->getDisplayName(); ?></h4>
									<p class="text-secondary mb-1"><?= Application::$app->user->getDisplayStatus(); ?></p>
									<p class="text-muted font-size-sm">Bay Area, San Francisco, CA</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-8">
					<div class="card">
						<div class="card-body">
							<div class="row mb-3">
								<div class="col-sm-3">
									<h6 class="mb-0">Full Name</h6>
								</div>
								<div class="col-sm-9 text-secondary">
                                <div class="row">
                                    <div class="col">
                                    <input type="text" name="firstname" value="<?= Application::$app->user->firstname;?>" class="form-control">
                                    <div class="invalid-feedback">
                                            
                                    </div>
                                    </div>
                                    <div class="col">
                                    <input type="text" name="lastname" value="<?= Application::$app->user->lastname;?>" class="form-control">
                                    <div class="invalid-feedback">
                                        
                                    </div>
                                    </div>
                                </div>
								</div>
							</div>
							<div class="row mb-3">
								<div class="col-sm-3">
									<h6 class="mb-0">Email</h6>
								</div>
								<div class="col-sm-9 text-secondary">
                                <input type="text" name="email" value="<?= Application::$app->user->email;?>" class="form-control">
                                    <div class="invalid-feedback">
                                        
                                    </div>
								</div>
							</div>
							<div class="row mb-3">
								<div class="col-sm-3">
									<h6 class="mb-0">Photo</h6>
								</div>
								<div class="col-sm-9 text-secondary">
                                    <input type="file" name="photo" value="<?= Application::$app->user->photo;?>" class="form-control">
                                    <div class="invalid-feedback">
                    
                                    </div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-3"></div>
								<div class="col-sm-9 text-secondary">
                                <button type="submit" class="btn btn-primary">Save changes</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php Form::end();?>