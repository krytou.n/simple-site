<?php

use app\core\form\Form;

?>

<h3>Form to create a new post</h3>
<?php

$this->title = 'Create post';

$form = Form::begin('', "post"); ?>
  <div class="row">
    <?php echo $form->field($model, 'title'); ?>
  </div>
  <div class="form-group">
    <label>Text</label><br>
    <textarea name="text" cols="60" rows="4"></textarea>
  </div>
  
  <button type="submit" class="btn btn-primary">Submit</button>
<?php Form::end();?>
