<?php

use app\core\Application;

function createLink($href, $text)
{
    if ($_SERVER['REQUEST_URI'] == $href) {
        $teg = "<span class=\"sr-only\">(current)</span>";
    } else {
        $teg = '';
    }
    echo "<li class=\"nav-item active\">" .
        "<a href = \"$href\" class = \"nav-link\">$text$teg</a>" .
        "</li>\n";
}
    createLink('/', 'Home');
    createLink('/post', 'Posts');
?>

<?php if (Application::isGuest()) : ?>
    <ul class="navbar-nav ml-auto">
        <?php createLink('/login', 'Login'); ?>
    <li class="nav-item">
        <a href = "/register" class = "nav-link">Regist</a>
    </li>
    </ul>
<?php else :    ?>
    <ul class="navbar-nav ml-auto">
        <?php createLink('/profile', 'Profile'); ?>
    <li class="nav-item">
        <a href = "/logout" class = "nav-link"> Logout</a>
    </li>
    </ul>
<?php endif; ?>
    